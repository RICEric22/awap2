from game import Game
import numpy as np

def compute_spread(game, my_number):
	N = game.dimension
	
	xmin = N+1
	xmax = -1
	ymin = N+1
	ymax = -1
	
	for i in  range(0,N*N):
		x = i / N
		y = i % N
		
		if game.grid[x][y] == my_number:
			xmin = min(xmin,x)
			ymin = min(ymin,y)
			xmax = max(xmax,x)
			ymax = max(ymax,y)
			
	return (xmax-xmin) * (ymax-ymin)
