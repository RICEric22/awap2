##############################################################################
# game.py - Responsible for generating moves to give to client.py            #
# Moves via stdout in the form of "# # # #" (block index, # rotations, x, y) #
# Important function is find_move, which should contain the main AI          #
##############################################################################

import sys
import json

# Simple point class that supports equality, addition, and rotations
class Point:
    x = 0
    y = 0

    # Can be instantiated as either Point(x, y) or Point({'x': x, 'y': y})
    def __init__(self, x=0, y=0):
        if isinstance(x, dict):
            self.x = x['x']
            self.y = x['y']
        else:
            self.x = x
            self.y = y

    def __add__(self, point):
        return Point(self.x + point.x, self.y + point.y)

    def __eq__(self, point):
        return self.x == point.x and self.y == point.y

    # rotates 90deg counterclockwise
    def rotate(self, num_rotations):
        if num_rotations == 1: return Point(-self.y, self.x)
        if num_rotations == 2: return Point(-self.x, -self.y)
        if num_rotations == 3: return Point(self.y, -self.x)
        return self

    def distance(self, point):
        return abs(point.x - self.x) + abs(point.y - self.y)

class Game:
    blocks = []
    grid = []
    bonus_squares = []
    my_number = -1
    dimension = -1 # Board is assumed to be square
    turn = -1

    def __init__(self, args):
        self.interpret_data(args)

    # find_move is your place to start. When it's your turn,
    # find_move will be called and you must return where to go.
    # You must return a tuple (block index, # rotations, x, y)
    def find_move(self):
        moves = []
        N = self.dimension
        best_move = (-1, -1, -1, -1)
        best_score = -10000
        for index, block in enumerate(self.blocks):
            for i in range(0, N * N):
                x = i / N
                y = i % N

                for rotations in range(0, 4):
                    new_block = self.rotate_block(block, rotations)
                    if self.can_place(new_block, Point(x, y)):
                        self.print_grid(self.grid)
                        self.print_grid(self.reachable_area())
                        return (index, rotations, x, y)

        return (0, 0, 0, 0)

    def score(self, index, rotations, x, y): 
        return self.reachable_area()

    def reachable_area(self):
        N = self.dimension
        reachable = [[0 for i in range(N)] for j in range(N)]
        for i in range(0, N * N):
            x = i / N
            y = i % N
            singleton = [Point(0,0)]
            if self.can_place(singleton, Point(x, y)) and reachable[i][j]==0:
                self.fill(reachable,i,j)
                
                
        return reachable
    
    def fill(self, reachable,i,j):
        if(i >= 0 and i < N and j >= 0 and j<N):
            reachable[i][j]=1
        if reachable[i+1][j]==-1:
            reachable[i+1][j]=self.my_number
            self.fill(reachable,i+1,j)
	if reachable[i-1][j]==-1:
            reachable[i-1][j]=self.my_number
            self.fill(reachable,i-1,j)
	if reachable[i][j+1]==-1:
            reachable[i][j+1]=self.my_number
            self.fill(reachable,i,j+1)
	if reachable[i][j-1]==-1:
            reachable[i][j-1]=self.my_number
            self.fill(reachable,i,j-1)
        
    def make_move(self, index, block, rotations, x, y):
        blocks0 = []
        blocks0.extend(self.blocks[:index])
        blocks0.extend(self.blocks[index+1:-1])
        
        N = self.dimension
        grid0 = []
        for i in range (0,N): 
            grid0.append([])
            for j in range (0,N):
                grid0[i].append(self.grid[i][j])

        for offset in block: 
            x0 = x + offset.x
            y0 = y + offset.y
            grid0[x0][y0] = self.my_number

        return (grid0, blocks0)

    def print_grid(self, grid):
        N = self.dimension
        for i in range(0,N):
            s = ''
            for j in range(0,N):
                s += (str(grid[i][j]) + ' ').rjust(3)
            print >> sys.stderr, s

    def print_blocks(self,blocks):
        for index, block in enumerate(self.blocks):
            print >> sys.stderr, '\n'
            s = ''
            B = []
            for i in range(0,7): 
                B.append([0,0,0,0,0,0,0])
                
            for p in block:
                B[p.x+2][p.y+2] = 1
                s += '(' + str(p.x) + ',' + str(p.y) + ') '
#            print >> sys.stderr, s
            
            s=''
            for i in range(0,7):
                s = ''
                for j in range(0,7): 
                    if B[i][j]==1:
                        s += 'x'
                    else:
                        s += ' '
                if s != '       ':
                    print >> sys.stderr, s


    # Checks if a block can be placed at the given point
    def can_place(self, block, point):
        onAbsCorner = False
        onRelCorner = False
        N = self.dimension - 1

        corners = [Point(0, 0), Point(N, 0), Point(N, N), Point(0, N)]
        corner = corners[self.my_number]

        for offset in block:
            p = point + offset
            x = p.x
            y = p.y
            if (x > N or x < 0 or y > N or y < 0 or self.grid[x][y] != -1 or
                (x > 0 and self.grid[x - 1][y] == self.my_number) or
                (y > 0 and self.grid[x][y - 1] == self.my_number) or
                (x < N and self.grid[x + 1][y] == self.my_number) or
                (y < N and self.grid[x][y + 1] == self.my_number)
            ): return False

            onAbsCorner = onAbsCorner or (p == corner)
            onRelCorner = onRelCorner or (
                (x > 0 and y > 0 and self.grid[x - 1][y - 1] == self.my_number) or
                (x > 0 and y < N and self.grid[x - 1][y + 1] == self.my_number) or
                (x < N and y > 0 and self.grid[x + 1][y - 1] == self.my_number) or
                (x < N and y < N and self.grid[x + 1][y + 1] == self.my_number)
            )

        if self.grid[corner.x][corner.y] < 0 and not onAbsCorner: return False
        if not onAbsCorner and not onRelCorner: return False

        return True

    # rotates block 90deg counterclockwise
    def rotate_block(self, block, num_rotations):
        return [offset.rotate(num_rotations) for offset in block]

    # updates local variables with state from the server
    def interpret_data(self, args):
        if 'error' in args:
            debug('Error: ' + args['error'])
            return

        if 'number' in args:
            self.my_number = args['number']

        if 'board' in args:
            self.dimension = args['board']['dimension']
            self.turn = args['turn']
            self.grid = args['board']['grid']
            self.blocks = args['blocks'][self.my_number]
            self.bonus_squares = args['board']['bonus_squares']

            for index, block in enumerate(self.blocks):
                self.blocks[index] = [Point(offset) for offset in block]

        if (('move' in args) and (args['move'] == 1)):
            send_command(" ".join(str(x) for x in self.find_move()))

    def is_my_turn(self):
        return self.turn == self.my_number

def get_state():
    return json.loads(raw_input())

def send_command(message):
    print message
    sys.stdout.flush()

def debug(message):
    send_command('DEBUG ' + str(message))

def main():
    setup = get_state()
    game = Game(setup)

    while True:
        state = get_state()
        game.interpret_data(state)

if __name__ == "__main__":
    main()
