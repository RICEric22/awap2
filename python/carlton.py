from game import Game, Point
from numpy import *

def evaluate(game):
    
    N = game.dimension - 1
    
    count = 0
    for i in range(0, N * N):
        x = i / N
        y = i % N
        
        if ((not onSide(game,x,y,N)) and onCorner(game,x,y,N) and game.grid[x][y]==-1):
            count += 1
    
    return count
        
def onSide(game, x, y, N):
    if (        (x > 0 and game.grid[x - 1][y] == game.my_number) or
                (y > 0 and game.grid[x][y - 1] == game.my_number) or
                (x < N and game.grid[x + 1][y] == game.my_number) or
                (y < N and game.grid[x][y + 1] == game.my_number)
            ): 
        return True
            
def onCorner(game, x, y, N):
    
    corners = [Point(0, 0), Point(N, 0), Point(N, N), Point(0, N)]
    corner = corners[game.my_number]
    
    if( (Point(x,y)==corner) or
        (x > 0 and y > 0 and game.grid[x - 1][y - 1] == game.my_number) or
        (x > 0 and y < N and game.grid[x - 1][y + 1] == game.my_number) or
        (x < N and y > 0 and game.grid[x + 1][y - 1] == game.my_number) or
        (x < N and y < N and game.grid[x + 1][y + 1] == game.my_number)
    ):
        return True
    

        
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    