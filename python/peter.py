##############################################################################
# game.py - Responsible for generating moves to give to client.py            #
# Moves via stdout in the form of "# # # #" (block index, # rotations, x, y) #
# Important function is find_move, which should contain the main AI          #
##############################################################################

import sys
import json


# Simple point class that supports equality, addition, and rotations
class Point:
    x = 0
    y = 0

    # Can be instantiated as either Point(x, y) or Point({'x': x, 'y': y})
    def __init__(self, x=0, y=0):
        if isinstance(x, dict):
            self.x = x['x']
            self.y = x['y']
        else:
            self.x = x
            self.y = y

    def __add__(self, point):
        return Point(self.x + point.x, self.y + point.y)

    def __eq__(self, point):
        return self.x == point.x and self.y == point.y

    # rotates 90deg counterclockwise
    def rotate(self, num_rotations):
        if num_rotations == 1: return Point(-self.y, self.x)
        if num_rotations == 2: return Point(-self.x, -self.y)
        if num_rotations == 3: return Point(self.y, -self.x)
        return self

    def distance(self, point):
        return abs(point.x - self.x) + abs(point.y - self.y)

class Game:
    blocks = []
    grid = []
    bonus_squares = []
    my_number = -1
    dimension = -1 # Board is assumed to be square
    turn = -1

    def __init__(self, args):
        self.interpret_data(args)

    def __init__(self, b, g, bs, mn, d, t):
        self.blocks = b
        self.grid = g
        self.bonus_squares = bs
        self.my_number = mn
        self.dimension = d
        self.turn = t

    # find_move is your place to start. When it's your turn,
    # find_move will be called and you must return where to go.
    # You must return a tuple (block index, # rotations, x, y)
    def find_move(self):
        moves = []
        N = self.dimension
        for index, block in enumerate(self.blocks):
            for i in range(0, N * N):
                x = i / N
                y = i % N

                for rotations in range(0, 4):
                    new_block = self.rotate_block(block, rotations)
                    if self.can_place(new_block, Point(x, y)):
                        (grid0, blocks0) = self.make_move(index,new_block,rotations,x,y)
                        
                        #self.print_grid(grid0)
                        #self.print_blocks(blocks0)

                        return (index, rotations, x, y)
        self.points(self.grid, self.my_number)
        return (0, 0, 0, 0)

    def floodFill(self, blockcells, grid, x, y, num):
        size = 0
        if not self.validFlood(blockcells, grid, x, y, num):
            return 0
        if (grid[x][y] == num):
            blockcells.append((x, y))
            size += 1
        size += self.floodFill(blockcells, grid, x-1, y, num);
        size += self.floodFill(blockcells, grid, x+1, y, num);
        size += self.floodFill(blockcells, grid, x, y-1, num);
        size += self.floodFill(blockcells, grid, x, y+1, num);
        return size

    def validFlood(self, blockcells, grid, x, y, num):
        N = self.dimension
        if (x >= 0 and y >= 0 and y <= N and x <= N):
            for cell in blockcells:
                if (x == cell[0] and y == cell[1]):
                    return 0
            if grid[x][y] == num:
                return 1
        return 0

    #checks how many points we have
    def points(self, grid, num):
        score = 0;
        N = self.dimension
        for i in range(0, N):
            for j in range(0, N):
                if (grid[i][j] == num):
                    score += 1
        for bonus in self.bonus_squares:
            if grid[bonus[0]][bonus[1]] == num:
                blockcells = []
                numFlood = self.floodFill(blockcells, grid,bonus[0],bonus[1],num)
                score += numFlood*2
        print >> sys.stderr, score
        return score
    
    def make_move(self, index, block, rotations, x, y):
        blocks0 = []
        blocks0.extend(self.blocks[:index])
        blocks0.extend(self.blocks[index+1:-1])
        
        N = self.dimension
        grid0 = []
        for i in range (0,N): 
            grid0.append([])
            for j in range (0,N):
                grid0[i].append(self.grid[i][j])

        for offset in block: 
            x0 = x + offset.x
            y0 = y + offset.y
            grid0[x0][y0] = self.my_number
        G = Game()
        return (grid0, blocks0)

    def print_grid(self, grid):
        N = self.dimension
        for i in range(0,N):
            s = ''
            for j in range(0,N):
                s += (str(grid[i][j]) + ' ').rjust(3)
            print >> sys.stderr, s
    
    def print_blocks(self,blocks):
        for index, block in enumerate(self.blocks):
            print >> sys.stderr, ''
            s = ''
            B = []
            for i in range(0,7): 
                B.append([0,0,0,0,0,0,0])
                
            for p in block:
                B[p.x+2][p.y+2] = 1
                s += '(' + str(p.x) + ',' + str(p.y) + ') '
#            print >> sys.stderr, s
            
            s=''
            for i in range(0,7):
                s = ''
                for j in range(0,7): 
                    if B[i][j]==1:
                        s += 'x'
                    else:
                        s += ' '
                if s != '       ':
                    print >> sys.stderr, s
            

    # Checks if a block can be placed at the given point
    def can_place(self, block, point):
        onAbsCorner = False
        onRelCorner = False
        N = self.dimension - 1

        corners = [Point(0, 0), Point(N, 0), Point(N, N), Point(0, N)]
        corner = corners[self.my_number]

        for offset in block:
            p = point + offset
            x = p.x
            y = p.y
            if (x > N or x < 0 or y > N or y < 0 or self.grid[x][y] != -1 or
                (x > 0 and self.grid[x - 1][y] == self.my_number) or
                (y > 0 and self.grid[x][y - 1] == self.my_number) or
                (x < N and self.grid[x + 1][y] == self.my_number) or
                (y < N and self.grid[x][y + 1] == self.my_number)
            ): return False

            onAbsCorner = onAbsCorner or (p == corner)
            onRelCorner = onRelCorner or (
                (x > 0 and y > 0 and self.grid[x - 1][y - 1] == self.my_number) or
                (x > 0 and y < N and self.grid[x - 1][y + 1] == self.my_number) or
                (x < N and y > 0 and self.grid[x + 1][y - 1] == self.my_number) or
                (x < N and y < N and self.grid[x + 1][y + 1] == self.my_number)
            )

        if self.grid[corner.x][corner.y] < 0 and not onAbsCorner: return False
        if not onAbsCorner and not onRelCorner: return False

        return True

    # rotates block 90deg counterclockwise
    def rotate_block(self, block, num_rotations):
        return [offset.rotate(num_rotations) for offset in block]

    # updates local variables with state from the server
    def interpret_data(self, args):
        if 'error' in args:
            debug('Error: ' + args['error'])
            return

        if 'number' in args:
            self.my_number = args['number']

        if 'board' in args:
            self.dimension = args['board']['dimension']
            self.turn = args['turn']
            self.grid = args['board']['grid']
            self.blocks = args['blocks'][self.my_number]
            self.bonus_squares = args['board']['bonus_squares']

            for index, block in enumerate(self.blocks):
                self.blocks[index] = [Point(offset) for offset in block]

        if (('move' in args) and (args['move'] == 1)):
            send_command(" ".join(str(x) for x in self.find_move()))
            
    def is_my_turn(self):
        return self.turn == self.my_number



def get_state():
    return json.loads(raw_input())

def send_command(message):
    print message
    sys.stdout.flush()

def debug(message):
    send_command('DEBUG ' + str(message))

def main():
    setup = get_state()
    game = Game(setup)

    while True:
        state = get_state()
        game.interpret_data(state)

if __name__ == "__main__":
    main()
