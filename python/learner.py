import numpy as np;

class sarsa_learner:
	def __init__(self):
		self.step_size = 0.1
		selg.discount = 0.8

	def featurize_state(state):
		return np.array(state)
		
	def save_parameters(self, filename):
		np.savetext(self.params)
		
	def load_parameters(self, filename):
		self.params = np.loadtext(filename)
		
	def init_state(state):
		self.state = featurize_state(state)
		
	def update_paramaters(new_state, scores):
		reward = scores[0] - max(scores[1:4])
		new_state = featurize_state(state)
		delta = reward + np.vdot(self.params, self.discount * new_state - self.state) 
		self.params += self.step_size * delta * self.state
		self.state = new_state
