import sys

def get_piece(target, blocks): 
	for index,block in enumerate(blocks): 
		if len(target) == len(block):
			for i in range(0,len(target)): 
				if(target[i].x != block[i].x or target[i].y != block[i].y):
					break;
				return index
	return -1

def reachable_area(G):
	N = G.dimension
	reachable = [[0 for i in range(N)] for j in range(N)]
	for i in range(0, N * N):
	    x = i / N
	    y = i % N
	    singleton = [Point(0,0)]
	    if G.can_place(singleton, Point(x, y)) and reachable[i][j]==0:
		G.fill(reachable,i,j)
	return reachable

def fill(G, reachable,i,j):
	if(i >= 0 and i < N and j >= 0 and j<N):
	    reachable[i][j]=1
	if reachable[i+1][j]==-1:
	    reachable[i+1][j]=G.my_number
	    G.fill(reachable,i+1,j)
	if reachable[i-1][j]==-1:
	    reachable[i-1][j]=G.my_number
	    G.fill(reachable,i-1,j)
	if reachable[i][j+1]==-1:
	    reachable[i][j+1]=G.my_number
	    G.fill(reachable,i,j+1)
	if reachable[i][j-1]==-1:
	    reachable[i][j-1]=self.my_number
	    self.fill(reachable,i,j-1)
